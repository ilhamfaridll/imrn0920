console.log("NOMOR 1")

function range(angka1, angka2) {
    var urutan = []
    if (angka1 <= angka2) {
        for (a = angka1; a <= angka2; a++) {
            urutan.push(a);
        } return urutan;
    } if (angka1 >= angka2) {
        for (a = angka1; a >= angka2; a--) {
            urutan.push(a);
        } return urutan;
    } else {
        return (-1);
    }
}
console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

console.log("==============================================")
console.log("NOMOR 2")

function rangeWithStep(startNum, finishNum, step) {
    var urutan2 = []
    if (startNum <= finishNum) {
        for (b = startNum; b <= finishNum; b += step) {
            urutan2.push(b);
        } return urutan2;
    } if (startNum >= finishNum) {
        for (b = startNum; b >= finishNum; b -= step) {
            urutan2.push(b);
        } return urutan2;
    } else {
        return urutan2;
    }
}
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

console.log("==============================================")
console.log("NOMOR 3")

function sum(num1 = 0, num2 = 0, step = 1) {
    var deret = [];
    var jumlah = 0;

    if (num1 < num2 && step > 0) {
        while (num1 <= num2) {
            deret.push(num1);
            num1 += step;
        }
    } else if (num1 > num2 && step > 0) {
        while (num2 <= num1) {
            deret.push(num2);
            num2 += step;
        }
    } else if (num1 == num2) {
        deret.push(num2);
    } else {
        return "invalid input!";
    }

    for (i = 0; i < deret.length; i++) {
        jumlah += deret[i];
    } return jumlah

}
console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum(0));

console.log("==============================================")
console.log("NOMOR 4")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling(rawData) {

    var output = "";

    for (i = 0; i < rawData.length; i++) {

        output += "Nomor ID: " + rawData[i][0] + "\n";
        output += "Nama Lengkap: " + rawData[i][1] + "\n";
        output += "TTL: " + rawData[i][2] + " " + rawData[i][3] + "\n";
        output += "Hobi: " + rawData[i][4] + "\n";
        output += "\n"

    }
    return output;
}
console.log(dataHandling(input))

console.log("==============================================")
console.log("NOMOR 5")

function balikKata(kata) {
    var stack = "";
    var panjangKata = kata.length - 1;

    for (i = panjangKata; i >= 0; i--) {
        stack += kata[i];
    }
    return stack;
}

console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

console.log("==============================================")
console.log("NOMOR 6")

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1999", "Membaca"];

function dataHandling2(dataInput) {

    dataInput.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    dataInput.splice(-1, 1, "Pria", "SMA Internasional Metro");

    console.log(dataInput);
    console.log("\n");

    var tanggal = dataInput[3].split("/");

    console.log(tanggal);
    console.log("\n");

    var bulan = dataInput[3].slice(3, 5);

    switch (bulan) {
        case "01":
            bulan = "Januari";
            break;
        case "02":
            bulan = "Februari";
            break;
        case "03":
            bulan = "Maret";
            break;
        case "04":
            bulan = "April";
            break;
        case "05":
            bulan = "Mei";
            break;
        case "06":
            bulan = "Juni";
            break;
        case "07":
            bulan = "Juli";
            break;
        case "08":
            bulan = "Agustus";
            break;
        case "09":
            bulan = "September";
            break;
        case "10":
            bulan = "Oktober";
            break;
        case "11":
            bulan = "November";
            break;
        default:
            bulan = "Desember";
            break;
    }
    console.log(tanggal.sort((a, b) => { return b - a }));
    console.log("\n");

    tanggal.splice(1, 1, bulan);
    var formattedDate = tanggal.join("-");

    console.log(formattedDate);
    console.log("\n");

    var nama = dataInput[1].slice(0, 14);
    console.log(nama);
}
dataHandling2(input);